import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span>
          &copy; 2019 <a href="https://storactive.ma/"> Storactive</a>
        </span>
        <span className="ml-auto" />
      </footer>
    );
  }
}

export default Footer;
