import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import {
  Badge,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";
const ParamSousFamille = () => {
  const [sousfamille, setSousFamille] = useState([]);
  const [usousfamille, setUsousfamille] = useState({
    id: "",
    nom: "",
    idfamille: ""
  });
  const [famille, setFamille] = useState([]);
  const [test, setTest] = useState("");
  const [etat, setEtat] = useState("add");

  let _formRef = null;
  useEffect(() => {
    fetch("/famille")
      .then(response => response.json())
      .then(data => {
        setFamille(data);
      });
  }, [test]);
  const selectfamille = event => {
    setTest(event.target.value);
    fetch(`/sousfamille/${event.target.value}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        setSousFamille(data);
      });
  };
  const handlesubmit = e => {
    e.preventDefault();
    if (e.target.lastClicked === "deletesousfamille") {
      fetch(`/sousfamille/${e.target.idsousfamille}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          fullsoufamille();
        });
    }
    if (e.target.lastClicked === "addsousfamille") {
      fetch("/sousfamille", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          nom: event.target.elements.nom.value,
          id_famille: event.target.elements.famille.value
        })
      })
        .then(res => res.json())
        .then(data => {
          window.alert(data);
          fullsoufamille();
        });
    }
    if (e.target.lastClicked === "updatesousfamille") {
      console.log(usousfamille);
      fetch(`/sousfamille/${usousfamille}`, {
        method: "PUT",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          id: usousfamille.id,
          nom: usousfamille.nom,
          idfamille: usousfamille.idfamille
        })
      })
        .then(response => response.json())
        .then(data => {
          console.log(data);
          fullsoufamille();
        });
    }
  };

  const fullsoufamille = () => {
    fetch(`/sousfamille/${test}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        setSousFamille(data);
      });
  };
  const block = () => {
    if (etat === "add") {
      return (
        <Button
          type="submit"
          size="sm"
          color="primary "
          className="mr-2"
          onClick={() => (_formRef.lastClicked = "addsousfamille")}
        >
          <i className="fa fa-dot-circle-o" /> nouveau
        </Button>
      );
    }
    if (etat === "update") {
      return (
        <React.Fragment>
          <Button
            type="submit"
            size="sm"
            color="primary "
            className="mr-2"
            onClick={() => (_formRef.lastClicked = "updatesousfamille")}
          >
            modifie
          </Button>
          <Button
            type="submit"
            size="sm"
            color="secondary "
            className="mr-2"
            onClick={() => {
              setEtat("add");
              setUsousfamille({ id: "", nom: "", idfamille: "" });
            }}
          >
            annuler
          </Button>
        </React.Fragment>
      );
    }
  };
  const handleFieldChange = event => {
    setUsousfamille({
      id: usousfamille.id,
      nom: event.target.value,
      idfamille: usousfamille.idfamille
    });
  };

  return (
    <Form
      encType="multipart/form-data"
      className="form-horizontal"
      name="form1"
      innerRef={ref => (_formRef = ref)}
      onSubmit={handlesubmit}
    >
      <Row>
        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> famille
            </CardHeader>
            <CardBody>
              <FormGroup row>
                <Col md="4">
                  <Label htmlFor="text-input"> famille</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    type="select"
                    name="famille"
                    id="select"
                    onChange={selectfamille}
                  >
                    <option className="hidden" selected>
                      Select famille
                    </option>
                    {famille.map((f, index) => (
                      <option key={index} value={f.id_famille}>
                        {f.nom}
                      </option>
                    ))}
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="4">
                  <Label htmlFor="text-input"> sous famille</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    class="form-control"
                    type="text"
                    id="text-input"
                    name="nom"
                    onChange={handleFieldChange}
                    value={usousfamille.nom}
                  />
                  <FormText color="muted" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="8" />
                <Col xs="12" md="8">
                  {block()}
                </Col>
              </FormGroup>
            </CardBody>
          </Card>
        </Col>

        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> liste des sousfamille
            </CardHeader>
            <CardBody>
              <Table responsive striped>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>nom</th>

                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {sousfamille.map((e, index) => (
                    <tr>
                      <td>{e.id_sousfamille}</td>
                      <td>{e.nom}</td>
                      <td>
                        <Button
                          type="submit"
                          size="sm"
                          color="danger "
                          className="mr-2"
                          onClick={() => {
                            if (window.confirm("vous étes sur?"))
                              (_formRef.lastClicked = "deletesousfamille"),
                                (_formRef.idsousfamille = e.id_sousfamille);
                          }}
                        >
                          <i className="icon-trash" />
                        </Button>
                        <button
                          type="submit"
                          size="sm"
                          color="success"
                          className="mr-2"
                          onClick={event => {
                            event.preventDefault();
                            setUsousfamille({
                              id: e.id_sousfamille,
                              nom: e.nom,
                              idfamille: e.id_famille
                            });
                            setEtat("update");
                          }}
                        >
                          <i className="icon-pencil" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};
export default ParamSousFamille;
