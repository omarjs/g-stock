import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import {
  Badge,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";
import { display } from "@material-ui/system";
const PramEntrepot = () => {
  const [entrepot, setEntrepot] = useState([]);

  const [uentrepot, setUentrepot] = useState({
    id: "",
    numero: "",
    nom: "",
    adresse: ""
  });
  const [etat, setEtat] = useState("add");
  const [test, setTest] = useState("");
  useEffect(() => {
    fetch("/entrepot")
      .then(response => response.json())
      .then(data => {
        setEntrepot(data);
      });
  }, [test]);

  const handlesubmit = e => {
    e.preventDefault();

    if (e.target.lastClicked === "addentrepot") {
      fetch("/entrepot", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          numero: event.target.elements.numero.value,
          name: event.target.elements.name.value,
          adresse: event.target.elements.adresse.value
        })
      })
        .then(res => res.json())
        .then(data => window.alert(data), setTest(Math.random()));
    }
    if (e.target.lastClicked === "deleteentrepot") {
      console.log(e.target.identrepot);

      fetch(`/entrepot/${e.target.identrepot}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        }
      })
        .then(res => res.json())
        .then(data => setTest(Math.random()));
    }
    if (e.target.lastClicked === "updateenterpot") {
      fetch(`/entrepot/${uentrepot}`, {
        method: "PUT",
        headers: {
          Accept: "application/json, text/plain, */ ",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          id: uentrepot.id,
          numero: uentrepot.numero,
          name: uentrepot.nom,
          adresse: uentrepot.adresse
        })
      })
        .then(response => response.json())
        .then(data => {
          console.log(data);
          setTest(Math.random());
        });
    }
  };
  const handleFieldChange = e => {
    if (e.target.name === "numero") {
      console.log(e.target.value);
      setUentrepot({
        id: uentrepot.id,
        numero: e.target.value,
        nom: uentrepot.nom,
        adresse: uentrepot.adresse
      });
    }
    if (e.target.name === "name") {
      console.log(e.target.name);
      setUentrepot({
        id: uentrepot.id,
        numero: uentrepot.numero,
        nom: e.target.value,
        adresse: uentrepot.adresse
      });
    }
    if (e.target.name === "adresse") {
      console.log(e.target.name);
      setUentrepot({
        id: uentrepot.id,
        numero: uentrepot.numero,
        nom: uentrepot.nom,
        adresse: e.target.value
      });
    }
  };

  let _formRef = null;
  const block = () => {
    if (etat === "add") {
      return (
        <Button
          type="submit"
          size="sm"
          color="primary "
          className="mr-2"
          onClick={() => (_formRef.lastClicked = "addentrepot")}
        >
          <i className="fa fa-dot-circle-o" /> nouveau
        </Button>
      );
    }
    if (etat === "update") {
      return (
        <React.Fragment>
          <Button
            type="submit"
            size="sm"
            color="primary "
            className="mr-2"
            onClick={() => (_formRef.lastClicked = "updateenterpot")}
          >
            modifie
          </Button>
          <Button
            type="submit"
            size="sm"
            color="secondary "
            className="mr-2"
            onClick={() => {
            setEtat("add");
            setUentrepot({ id: "", numero: "", nom: "", adresse: "" });
            }}
          >
            annuler
          </Button>
        </React.Fragment>
      );
    }
  };

  return (
    <Form
      encType="multipart/form-data"
      className="form-horizontal"
      name="form1"
      innerRef={ref => (_formRef = ref)}
      onSubmit={handlesubmit}
    >
      <Row>
        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> Entrepot
            </CardHeader>
            <CardBody>
              <FormGroup row>
                <Col md="2">
                  <Label htmlFor="text-input"> Numéro</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    class="form-control"
                    type="text"
                    id="text-input"
                    name="numero"
                    placeholder="numero"
                    value={uentrepot.numero}
                    onChange={handleFieldChange}
                    
                  />
                  <FormText color="muted" />
                </Col>
                <Col md="2">
                  <Label htmlFor="text-input"> Nom</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    type="text"
                    id="text-input"
                    name="name"
                    placeholder="nom"
                    onChange={handleFieldChange}
                    value={uentrepot.nom}
                  />
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="2">
                  <Label htmlFor="textarea-input">Adresse</Label>
                </Col>
                <Col xs="12" md="6">
                  <Input
                    type="textarea"
                    rows="2"
                    id="textarea-input"
                    name="adresse"
                    onChange={handleFieldChange}
                    value={uentrepot.adresse}
                  />
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col xs="12" md="6">
                  {block()}
                </Col>
              </FormGroup>
            </CardBody>
          </Card>
        </Col>

        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> liste des Entrepots
            </CardHeader>
            <CardBody>
              <Table responsive striped>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Numero</th>
                    <th>Nom</th>
                    <th>Adresse</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {entrepot.map((e, index) => (
                    <tr key={e.id_entrepot}>
                      <td>{e.id_entrepot}</td>
                      <td>{e.numero}</td>
                      <td>{e.name}</td>
                      <td>{e.adresse}</td>
                      <td className="w-25" key={e.id_entrepot}>
                        <Button
                          type="submit"
                          size="sm"
                          color="danger "
                          className="mr-2"
                          name={e.id_entrepot}
                          //   () => (_formRef.lastClicked = "deleteentrepot")
                          onClick={(e) => {
                            if (window.confirm("vous étes sur?"))
                            { (_formRef.lastClicked = "deleteentrepot"),
                                (_formRef.identrepot = e.id_entrepot);
                          }
                          else
                          {
                            _formRef.lastClicked = ""
                          }
                          }}
                        >
                          <i className="icon-trash" />
                        </Button>
                        <button
                          type="submit"
                          size="sm"
                          color="success"
                          className="mr-2"
                          name={e.id_entrepot}
                          onClick={() => {
                            setUentrepot({
                              id: e.id_entrepot,
                              numero: e.numero,
                              nom: e.name,
                              adresse: e.adresse
                            });
                            _formRef.lastClicked = "";
                            setEtat("update");
                          }}
                        >
                          <i className="icon-pencil" name={e.id_entrepot} />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};
export default PramEntrepot;
