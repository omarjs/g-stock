import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import {
  Badge,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";
const ParamFamille = () => {
  const [famille, setFamille] = useState([]);
  const [ufamille, setUfamille] = useState({ id: "", nom: "" });
  const [etat, setEtat] = useState("add");
  const [test, setTest] = useState("");
  useEffect(() => {
    fetch("/famille")
      .then(response => response.json())
      .then(data => {
        setFamille(data);
      });
  }, [test]);

  const handlesubmit = e => {
    e.preventDefault();
    if (e.target.lastClicked === "addfamille") {
      fetch("/famille", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          nom: event.target.elements.nom.value
        })
      })
        .then(res => res.json())
        .then(data => window.alert(data), setTest(Math.random()));
    }

    if (e.target.lastClicked === "deletefamille") {
      console.log(e.target.idfamille);
      fetch(`/famille/${e.target.idfamille}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        }
      })
        .then(res => res.json())
        .then(data => console.log(data), setTest(Math.random()));
    }
    if (e.target.lastClicked === "updatefamille") {
      fetch(`/famille/${ufamille}`, {
        method: "PUT",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          id: ufamille.id,
          nom: ufamille.nom
        })
      })
        .then(response => response.json())
        .then(data => {
          console.log(data);
          setTest(Math.random());
        });
    }
  };
  const handleFieldChange = event => {
    setUfamille({ id: ufamille.id, nom: event.target.value });
  };
  const block = () => {
    if (etat === "add") {
      return (
        <Button
          type="submit"
          size="sm"
          color="primary "
          className="mr-2"
          onClick={() => (_formRef.lastClicked = "addfamille")}
        >
          <i className="fa fa-dot-circle-o" /> nouveau
        </Button>
      );
    }
    if (etat === "update") {
      return (
        <React.Fragment>
          <Button
            type="submit"
            size="sm"
            color="primary "
            className="mr-2"
            onClick={() => (_formRef.lastClicked = "updatefamille")}
          >
            modifie
          </Button>
          <Button
            type="submit"
            size="sm"
            color="secondary "
            className="mr-2"
            onClick={() => {
              setEtat("add");
              setUfamille({ id: "", nom: "" });
            }}
          >
            annuler
          </Button>
        </React.Fragment>
      );
    }
  };
  let _formRef = null;
  return (
    <Form
      encType="multipart/form-data"
      className="form-horizontal"
      name="form1"
      innerRef={ref => (_formRef = ref)}
      onSubmit={handlesubmit}
    >
      <Row>
        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> famille
            </CardHeader>
            <CardBody>
              <FormGroup row>
                <Col md="4">
                  <Label htmlFor="text-input"> nom de la famille</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    class="form-control"
                    type="text"
                    id="text-input"
                    name="nom"
                    value={ufamille.nom}
                    onChange={handleFieldChange}
                    placeholder="nom"
                  />
                  <FormText color="muted" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="4" />
                <Col xs="12" md="8">
                  {block()}
                </Col>
              </FormGroup>
            </CardBody>
          </Card>
        </Col>

        <Col xs="12" lg="6">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> liste des famille
            </CardHeader>
            <CardBody>
              <Table responsive striped>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>nom</th>

                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {famille.map((e, index) => (
                    <tr>
                      <td>{e.id_famille}</td>
                      <td>{e.nom}</td>

                      <td>
                        <Button
                          type="submit"
                          size="sm"
                          color="danger "
                          className="mr-2"
                          onClick={() => {
                            if (window.confirm("vous étes sur?"))
                              (_formRef.lastClicked = "deletefamille"),
                                (_formRef.idfamille = e.id_famille);
                          }}
                        >
                          <i className="icon-trash" />
                        </Button>
                        <button
                          type="submit"
                          size="sm"
                          color="success"
                          className="mr-2"
                          onClick={() => {
                            setUfamille({ id: e.id_famille, nom: e.nom });
                            _formRef.lastClicked = "";
                            setEtat("update");
                          }}
                        >
                          <i className="icon-pencil" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};
export default ParamFamille;
