import React, { useEffect, useState } from "react";
import ReactDatatable from "@ashvin27/react-datatable";
import Image from "material-ui-image";
import {
  Row,
  Col,
  Button,
  NavLink,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Input
} from "reactstrap";
import "./style.css";

const Mouvement = props => {
  const [mouvements, setMouvements] = useState([]);
  const [article ,setArticle]=useState({});
  useEffect(() => {
    fetch(`/mouvement/${props.match.params.id}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {setMouvements(data)});
      fetch(`/article/display/${props.match.params.id}`)
       .then(response=>response.json())
       .then(data=>setArticle(data))
    
    
    },[]);
  let columns = [
    {
      key: "id_mouvement",
      text: "id_mouvement",
      className: "name",
      align: "center",
      sortable: true
    },
    {
      key: "type",
      text: "type mouvement",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "quantite",
      text: "quantité",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "date",
      text: "date_mouvement",
      className: "postcode",
      align: "center",
      sortable: true
    }
  ];
  let config = {
    // page_size: 10,
    //length_menu: [10, 20, 50],
    show_pagination: false,
    button: {
      excel: false,
      print: false
    }
  };
  var somme=mouvements.reduce((acc,element)=>{
    if(element.type=='entree')
  { return acc+element.quantite;}
  else
  {
    return acc-element.quantite;
  }
  },0)
  console.log(somme);
  return (
    <React.Fragment>
      <NavLink href="/base/consultation">back</NavLink>
      <div className="row register-form  justify-content-center col-md-12  ">
      
        <div className="col-md-4">
          <Image src={`/public/${article.image}`}/>
        </div>
        <div className="col-md-8 ">
          <div className="form-group mb-3">
            <ListGroup>
              <ListGroupItem>
                <Label>Designation : </Label>
                <b> {article.designation}</b>
              </ListGroupItem>
              <ListGroupItem>
                <Label>Quantité : </Label>
                 <b>{article.quantite}</b>
              </ListGroupItem>
              <ListGroupItem>quantite réelle :   <b> {somme}</b> </ListGroupItem>
          
              <ListGroupItem>
                <Label>Date : </Label>
                <b>{article.date}</b>
              </ListGroupItem>
            </ListGroup>
          </div>
        </div>
        <div className="col-md-12" />
      </div>

      <div className="card w-100 ">
        <div className="card-body ">
          <ReactDatatable
            className="table col text-center"
            records={mouvements}
            columns={columns}
            config={config}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
export default Mouvement;
