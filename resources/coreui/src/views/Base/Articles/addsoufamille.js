import React from "react";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";
const AjouterSousFamille = props => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="6" md="12">
          <Card>
            <CardHeader>
              <strong>Ajouter</strong> Nouveau Sous Famille
            </CardHeader>
            <CardBody>
              <FormGroup row>
                <Col md="2">
                  <Label htmlFor="select">famille</Label>
                </Col>
                <Col xs="12" md="3">
                  <Input type="select" name="idsousfamille" id="select">
                    <option className="hidden" selected>
                      Select famille
                    </option>
                    {props.famille.map((f, index) => (
                      <option key={index} value={f.id_famille}>
                        {f.nom}
                      </option>
                    ))}
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                  <Label htmlFor="text-input"> nom</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    class="form-control"
                    type="text"
                    id="text-input"
                    name="nomsousfamille"
                    placeholder="nom"
                  />
                  <FormText color="muted" />
                </Col>
                <Button
                  type="submit"
                  size="sm"
                  color="primary "
                  className="mr-2"
                  onClick={() =>
                    (props._formRef.lastClicked = "addsousfamille")
                  }
                >
                  <i className="fa fa-dot-circle-o" /> Save
                </Button>
              </FormGroup>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};
export default AjouterSousFamille;
