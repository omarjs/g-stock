import React, { useState, useEffect } from "react";
import MaterialTable from "material-table";
import "bootstrap/dist/css/bootstrap.css";
import AjouterEntrepot from "./addentrepot";
import AjouterFamille from "./addfamille";
import AjouterSousFamille from "./addsoufamille";
import Popup from "reactjs-popup";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const Forms = () => {
  const [entrepot, setEntrepot] = useState([]);

  const [alert, setAlert] = useState("");
  const [test, setTest] = useState("");
  const [file, setFile] = useState({ imagename: "", imagefile: "" });
  const [article, setArticle] = useState([]);
  const [famille, setFamille] = useState([]);
  const [sousfamille, setSousFamille] = useState([]);

  const [columns, setColumns] = useState([
    { title: "ref", field: "ref" },
    { title: "designation", field: "designation" },
    { title: "marque", field: "marque" },
    { title: "date d'entrée", field: "date", type: "date" },
    { title: "quantité", field: "quantite", type: "numeric" },
    { title: " alert stock", field: "stock", type: "numeric" }
  ]);
  const handlesubmit = e => {
    e.preventDefault();

    if (e.target.lastClicked === "addarticle") {
      setArticle([
        ...article,
        {
          [event.target.elements.ref.name]: event.target.elements.ref.value,
          [event.target.elements.marque.name]:
            event.target.elements.marque.value,
          [event.target.elements.unite.name]: event.target.elements.unite.value,
          [event.target.elements.quantite.name]:
            event.target.elements.quantite.value,
          [event.target.elements.designation.name]:
            event.target.elements.designation.value,
          [event.target.elements.date.name]: event.target.elements.date.value,
          [event.target.elements.entrepot.name]:
            event.target.elements.entrepot.value,
          [event.target.elements.sousfamille.name]:
            event.target.elements.sousfamille.value,
          [event.target.elements.famille.name]:
            event.target.elements.famille.value,
          [event.target.elements.stock.name]: event.target.elements.stock.value,
          imagename: file.imagename,
          imagefile: file.imagefile
        }
      ]);
    }
    if (e.target.lastClicked === "addentrepot") {
      fetch("/entrepot", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          numero: event.target.elements.numero.value,
          name: event.target.elements.name.value,
          adresse: event.target.elements.adresse.value
        })
      })
        .then(res => res.json())
        .then(
          data => window.alert(data),
          setTest("test")
          /* setEntrepot([
            ...entrepot
            /* {
              [event.target.elements.numero.name]:
                event.target.elements.numero.value,
              [event.target.elements.name.name]:
                event.target.elements.name.value,
              [event.target.elements.adresse.name]:
                event.target.elements.adresse.value
            }
          ])*/
        );
    }
    if (e.target.lastClicked === "addfamille") {
      fetch("/famille", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          nom: event.target.elements.nomfamille.value
        })
      })
        .then(res => res.json())
        .then(data => window.alert(data), setTest("test"));
    }

    if (e.target.lastClicked === "testphoto") {
      fetch("/article", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          imagefile: file,
          imagename: e.target.elements.image.files[0].name
        })
      })
        .then(res => res.json())
        .then(data => console.log(data));
    }
  };

  useEffect(() => {
    fetch("/entrepot")
      .then(response => response.json())
      .then(data => {
        setEntrepot(data);
      });
    fetch("/famille")
      .then(response => response.json())
      .then(data => {
        setFamille(data);
      });
  }, [test]);

  const selectfamille = event => {
    fetch(`/sousfamille/${event.target.value}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        setSousFamille(data);
      });
  };
  const sauvegarder = event => {
    event.preventDefault();
    if (article && article.length) {
      fetch("/article", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({ article })
      })
        .then(res => res.json())
        .then(data => window.alert(data), setArticle([]));
    }
  };
  const reftest = event => {
    let param;
    if (event.target.value == "") {
      param = "null";
    } else {
      param = event.target.value;
    }

    fetch(`/article/${param}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data === "no") {
          setAlert("exist deja");
        } else {
          setAlert("");
        }
      });
  };
  const ajouter = event => {
    event.preventDefault();
  };

  let fileReader;
  let namepicture;
  const filechange = e => {
    fileReader = new FileReader();
    fileReader.readAsDataURL(e.target.files[0]);
    namepicture = e.target.files[0].name;
    fileReader.onloadend = e => {
      setFile({
        imagename: namepicture,
        imagefile: e.target.result
      });
    };
  };
  let _formRef = null;

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="6" md="12">
          <Card>
            <CardHeader>
              <strong>Ajouter</strong> Nouveau Article
            </CardHeader>
            <CardBody>
              <Form
                encType="multipart/form-data"
                className="form-horizontal"
                name="form1"
                innerRef={ref => (_formRef = ref)}
                onSubmit={handlesubmit}
              >
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> ref</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      class="form-control"
                      type="text"
                      id="text-input"
                      name="ref"
                      placeholder="reférence"
                      onBlur={reftest}
                    />
                    <FormText color="muted">{alert}</FormText>
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"> marque</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="text"
                      id="text-input"
                      name="marque"
                      placeholder="marque"
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> quantité</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input type="number" id="text-input" name="quantite" />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"> alert stock</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input type="number" id="text-input" name="stock" />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> date d'entrée</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input type="date" id="text-input" name="date" />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="select"> unité</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input type="select" name="unite" id="select">
                      <option value="piece"> piece</option>
                      <option value="metre">métre</option>
                      <option value="kg"> kg</option>
                      <option value="boite"> boite</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="select">famille</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input
                      type="select"
                      name="famille"
                      id="select"
                      onChange={selectfamille}
                    >
                      <option className="hidden" selected>
                        Select famille
                      </option>
                      {famille.map((f, index) => (
                        <option key={index} value={f.id_famille}>
                          {f.nom}
                        </option>
                      ))}
                    </Input>
                  </Col>
                  <Col className="pl-1" md="1">
                    <Popup
                      trigger={
                        <Button color="link" size="sm">
                          Nouveau
                        </Button>
                      }
                      modal
                    >
                      {close => (
                        <div>
                          <a className="close" onClick={close}>
                            &times;
                          </a>
                          <AjouterFamille _formRef={_formRef} />
                        </div>
                      )}
                    </Popup>
                  </Col>
                  <Col md="2">
                    <Label htmlFor="textarea-input">designation</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      name="designation"
                      type="textarea"
                      rows="2"
                      id="textarea-input"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="select">sous famille</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input type="select" name="sousfamille" id="select">
                      {sousfamille.map((s, index) => (
                        <option key={index} value={s.id_sousfamille}>
                          {s.nom}
                        </option>
                      ))}
                    </Input>
                  </Col>
                  <Col className="pl-1" md="1">
                    <Popup
                      trigger={
                        <Button color="link" size="sm">
                          Nouveau
                        </Button>
                      }
                      modal
                    >
                      {close => (
                        <div>
                          <a className="close" onClick={close}>
                            &times;
                          </a>
                          <AjouterSousFamille
                            _formRef={_formRef}
                            famille={famille}
                          />
                        </div>
                      )}
                    </Popup>
                  </Col>
                  <Col md="2">
                    <Label htmlFor="select"> entrepot</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input type="select" name="entrepot" id="select">
                      {entrepot.map((e, index) => (
                        <option key={index} value={e.id_entrepot}>
                          {e.name}
                        </option>
                      ))}
                    </Input>
                  </Col>
                  <Col className="pl-1" md="1">
                    <Popup
                      trigger={
                        <Button color="link" size="sm">
                          Nouveau
                        </Button>
                      }
                      modal
                    >
                      {close => (
                        <div>
                          <a className="close" onClick={close}>
                            &times;
                          </a>
                          <AjouterEntrepot _formRef={_formRef} />
                        </div>
                      )}
                    </Popup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="file-input">File input</Label>
                  </Col>
                  <Col xs="12" md="5">
                    <Input
                      type="file"
                      name="image"
                      accept=".png"
                      onChange={e => filechange(e)}
                    />
                  </Col>
                </FormGroup>
                <Button
                  type="submit"
                  size="sm"
                  color="primary "
                  className="mr-2"
                  onClick={() => (_formRef.lastClicked = "addarticle")}
                >
                  <i className="fa fa-dot-circle-o" /> Ajouter
                </Button>
                <Button
                  type="submit"
                  size="sm"
                  color="primary "
                  className="mr-2"
                  onClick={() => (_formRef.lastClicked = "testphoto")}
                >
                  <i className="fa fa-dot-circle-o" /> test photo
                </Button>
                <Button
                  type="reset"
                  size="sm"
                  color="danger"
                  onClick={() => {
                    setAlert("");
                  }}
                >
                  <i className="fa fa-ban" /> Reset
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col xs="12" md="12">
          <Card>
            <CardHeader>
              <Button
                type="submit"
                size="sm"
                color="primary"
                onClick={sauvegarder}
              >
                <i className="fa fa-dot-circle-o" /> Valider
              </Button>
            </CardHeader>
            <CardBody>
              <MaterialTable
                title=" liste des article"
                columns={columns}
                data={article}
                editable={{
                  onRowAdd: newData =>
                    new Promise(resolve => {
                      setTimeout(() => {
                        resolve();
                        const data = [...article];
                        data.push(newData);
                        setArticle(data);
                      }, 600);
                    }),
                  onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                      setTimeout(() => {
                        resolve();
                        const data = [...article];
                        data[data.indexOf(oldData)] = newData;
                        setArticle(data);
                      }, 400);
                    }),
                  onRowDelete: oldData =>
                    new Promise(resolve => {
                      setTimeout(() => {
                        resolve();
                        const data = [...article];
                        data.splice(data.indexOf(oldData), 1);
                        setArticle(data);
                      }, 400);
                    })
                }}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Forms;
