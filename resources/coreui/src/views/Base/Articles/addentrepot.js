import React, { useState } from "react";

const AjouterEntrepot = props => {
  return (
    <div className="card w-60 ">
      <div className="card-body ">
        <h5 className="card-title text-center mb-2">Ajouter E ntrepot</h5>

        <div className="row register-form  justify-content-center  ">
          <div className="col-md-6">
            <div className="form-group mb-1">
              <input
                type="text"
                className="form-control"
                placeholder="numero"
                name="numero"
              />
            </div>
            <div className="form-group mb-1">
              <input
                type="text"
                className="form-control"
                placeholder="nom"
                name="name"
              />
            </div>
          </div>
          <div className="col-md-6 ">
            <div className="form-group mb-3">
              <textarea
                class="form-control"
                name="adresse"
                rows="3"
                placeholder="adresse"
              />
            </div>

            <input
              type="submit"
              className="btn btn-primary"
              value="save"
              onClick={() => (props._formRef.lastClicked = "addentrepot")}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default AjouterEntrepot;
