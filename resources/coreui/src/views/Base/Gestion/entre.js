import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Badge,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const Entre = props => {
  const [mouvements, setMouvements] = useState([]);
  useEffect(() => {
    console.log(props.article.id_article);
    fetch(`/mouvement/${props.article.id_article}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(res => res.json())
      .then(data => {
        setMouvements(data);
      });
  }, []);
  var somme = mouvements.reduce((acc, element) => {
    if (element.type == "entree") {
      return acc + element.quantite;
    } else {
      return acc - element.quantite;
    }
  }, 0);
  console.log(somme);
  const handlesubmit = e => {
    e.preventDefault();

    fetch("/mouvement", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */",
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        type: "entre",
        date: e.target.elements.date.value,
        quantite: e.target.elements.quantite.value,
        idarticle: props.article.id_article
      })
    })
      .then(res => res.json())
      .then(data => console.log(data));
  };

  return (
    <React.Fragment>
      <Form
        encType="multipart/form-data"
        className="form-horizontal"
        name="form1"
        onSubmit={handlesubmit}
      >
        <Card>
          <CardHeader>
            Alimentation du stock: {props.article.ref} --{" "}
            {props.article.designation} -- Quantité:
            {props.article.quantite}
          </CardHeader>
          <CardBody>
            <div className="animated fadeIn">
              <FormGroup row>
                <Col md="6">
                  <Label htmlFor="text-input">
                    quantité restante dans le stock:
                  </Label>
                </Col>
                <Col md="2">
                  <Label htmlFor="text-input">{somme}</Label>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                  <Label htmlFor="text-input"> quantité</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input
                    type="number"
                    id="text-input"
                    name="quantite"
                    max={somme}
                  />
                </Col>
                <Col md="2">
                  <Label htmlFor="text-input"> date de sortie</Label>
                </Col>
                <Col xs="12" md="4">
                  <Input type="date" name="date" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                  <Button
                    type="submit"
                    size="sm"
                    color="primary "
                    className="mr-2"
                  >
                    <i className="fa fa-dot-circle-o" /> valider
                  </Button>
                </Col>
              </FormGroup>
            </div>
          </CardBody>
        </Card>
      </Form>
    </React.Fragment>
  );
};
export default Entre;
