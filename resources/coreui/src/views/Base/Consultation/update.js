import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const UpdateArticle = props => {
  const [article, setArticle] = useState({});
  const [entrepot, setEntrepot] = useState([]);
  const [famille, setFamille] = useState([]);
  const [sousfamille, setSousFamille] = useState([]);
  useEffect(() => {
    setArticle(props.articles);
    fetch("/entrepot")
      .then(response => response.json())
      .then(data => {
        setEntrepot(data);
      });
    fetch("/famille")
      .then(response => response.json())
      .then(data => {
        setFamille(data);
      });
    fetch("/sousfamille")
      .then(response => response.json())
      .then(data => {
        setSousFamille(data);
      });
  }, []);
  const selectfamille = event => {
    fetch(`/sousfamille/${event.target.value}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        setSousFamille(data);
      });
  };
  const handlesubmit = e => {
    if (e.target.lastClicked == "update") {
      e.preventDefault();
      fetch(`/article/${e.target.lastClicked}`, {
        method: "PUT",
        headers: {
          Accept: "application/json, text/plain, */ ",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          id: article.id_article,
          ref: article.ref,
          marque: event.target.elements.marque.value,
          quantite: event.target.elements.quantite.value,
          alertstock: event.target.elements.stock.value,
          date: event.target.elements.date.value,
          unite: event.target.elements.unite.value,
          famille: event.target.elements.famille.value,
          sousfamille: event.target.elements.sousfamille.value,
          designation: event.target.elements.designation.value,
          entrepot: event.target.elements.entrepot.value
        })
      })
        .then(response => response.json())
        .then(data => {
          window.alert(data);
        });
    }
    if ((e.target.lastClicked = "delete")) {
      console.log(e.target.lastClicked);
    }
  };
  let _formRef = null;
  return (
    <Form
      encType="multipart/form-data"
      className="form-horizontal"
      name="form1"
      innerRef={ref => (_formRef = ref)}
      onSubmit={handlesubmit}
    >
      <div className="animated fadeIn">
        <Row>
          <Col xs="6" md="12">
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> ref</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      class="form-control"
                      type="text"
                      id="text-input"
                      name="ref"
                      placeholder="reférence"
                      value={article.ref}
                      onChange={e => {
                        setArticle({
                          alertstock: article.alertstock,
                          id_article: article.id_article,
                          designation: article.designation,
                          quantite: article.quantite,
                          id_sousfamille: article.id_sousfamille,
                          id_entrepot: article.id_entrepot,
                          marque: article.marque,
                          unite: article.unite,
                          date: article.date,
                          ref: e.target.value
                        });
                      }}
                    />
                    <FormText color="muted" />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"> marque</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="text"
                      id="text-input"
                      name="marque"
                      placeholder="marque"
                      value={article.marque}
                      onChange={e => {
                        setArticle({
                          alertstock: article.alertstock,
                          id_article: article.id_article,
                          designation: article.designation,
                          quantite: article.quantite,
                          id_sousfamille: article.id_sousfamille,
                          id_entrepot: article.id_entrepot,
                          unite: article.unite,
                          date: article.date,
                          ref: article.ref,
                          marque: e.target.value
                        });
                      }}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> quantité</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="number"
                      id="text-input"
                      name="quantite"
                      value={article.quantite}
                      onChange={e => {
                        setArticle({
                          alertstock: article.alertstock,
                          id_article: article.id_article,
                          designation: article.designation,
                          id_sousfamille: article.id_sousfamille,
                          id_entrepot: article.id_entrepot,
                          marque: article.marque,
                          unite: article.unite,
                          date: article.date,
                          ref: article.ref,
                          quantite: e.target.value
                        });
                      }}
                    />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"> alert stock</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="number"
                      id="text-input"
                      name="stock"
                      value={article.alertstock}
                      onChange={e => {
                        setArticle({
                          id_article: article.id_article,
                          designation: article.designation,
                          quantite: article.quantite,
                          id_sousfamille: article.id_sousfamille,
                          id_entrepot: article.id_entrepot,
                          marque: article.marque,
                          unite: article.unite,
                          date: article.date,
                          ref: article.ref,
                          alertstock: e.target.value
                        });
                      }}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> date d'entrée</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="date"
                      id="text-input"
                      name="date"
                      value={article.date}
                      onChange={e => {
                        setArticle({
                          alertstock: article.alertstock,
                          id_article: article.id_article,
                          designation: article.designation,
                          quantite: article.quantite,
                          id_sousfamille: article.id_sousfamille,
                          id_entrepot: article.id_entrepot,
                          marque: article.marque,
                          unite: article.unite,
                          ref: article.ref,
                          date: e.target.value
                        });
                      }}
                    />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="select"> unité</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input type="select" name="unite" id="select">
                      <option value="piece"> piece</option>
                      <option value="metre">métre</option>
                      <option value="kg"> kg</option>
                      <option value="boite"> boite</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="select">famille</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input
                      type="select"
                      name="famille"
                      id="select"
                      onChange={selectfamille}
                    >
                      {famille.map((f, index) => {
                        if (article.id_famille == f.id_famille) {
                          return (
                            <option key={index} value={f.id_famille} selected>
                              {f.nom}
                            </option>
                          );
                        } else {
                          return (
                            <option key={index} value={f.id_famille}>
                              {f.nom}
                            </option>
                          );
                        }
                      })}
                    </Input>
                  </Col>

                  <Col md="2">
                    <Label htmlFor="textarea-input">designation</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      name="designation"
                      type="textarea"
                      rows="2"
                      id="textarea-input"
                      value={article.designation}
                      onChange={e => {
                        setArticle({
                          alertstock: article.alertstock,
                          id_article: article.id_article,
                          quantite: article.quantite,
                          id_sousfamille: article.id_sousfamille,
                          id_entrepot: article.id_entrepot,
                          marque: article.marque,
                          unite: article.unite,
                          date: article.date,
                          ref: article.ref,
                          designation: e.target.value
                        });
                      }}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="select">sous famille</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input type="select" name="sousfamille" id="select">
                      {sousfamille.map((s, index) => {
                        if (article.id_sousfamille == s.id_sousfamille) {
                          return (
                            <option
                              key={index}
                              value={s.id_sousfamille}
                              selected
                            >
                              {s.nom}
                            </option>
                          );
                        } else {
                          return (
                            <option key={index} value={s.id_sousfamille}>
                              {s.nom}
                            </option>
                          );
                        }
                      })}
                    </Input>
                  </Col>

                  <Col md="2">
                    <Label htmlFor="select"> entrepot</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input type="select" name="entrepot" id="select">
                      {entrepot.map((e, index) => {
                        if (article.id_entrepot == e.id_entrepot) {
                          return (
                            <option key={index} value={e.id_entrepot} selected>
                              {e.name}
                            </option>
                          );
                        } else {
                          return (
                            <option key={index} value={e.id_entrepot}>
                              {e.name}
                            </option>
                          );
                        }
                      })}
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="file-input">File input</Label>
                  </Col>
                  <Col xs="12" md="5">
                    <Input type="file" name="image" accept=".png" />
                  </Col>
                </FormGroup>
                <Button
                  type="submit"
                  size="sm"
                  color="primary "
                  className="mr-2"
                  onClick={() => (_formRef.lastClicked = "update")}
                >
                  <i className="fa fa-dot-circle-o" /> valider modification
                </Button>

                <Button
                  type="reset"
                  size="sm"
                  color="danger"
                  onClick={() => (_formRef.lastClicked = "update")}
                >
                  <i className="fa fa-ban" /> Reset
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </Form>
  );
};

export default UpdateArticle;
