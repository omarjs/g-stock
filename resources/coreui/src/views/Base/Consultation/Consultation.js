import React, { useState, useEffect, Fragment } from "react";
import Popup from "reactjs-popup";
import UpdateArticle from "./update";
import Mouvement from "./mouvement";
import Pagination from "react-js-pagination";
import ReactDatatable from "@ashvin27/react-datatable";
import {
  Badge,
  Media,
  Row,
  Button,
  NavLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Col,
  NavLinkn,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Label,
  Input,
  PaginationItem,
  PaginationLink
} from "reactstrap";

let config = {
  // page_size: 10,
  //length_menu: [10, 20, 50],
  show_pagination: false,
  button: {
    excel: true,
    print: true
  }
};
const editRecord = record => {
  console.log("Edit Record", record);
};

const Consultation = () => {
  const [article, setArticle] = useState([]);
  const [detail, setDetail] = useState({
    activePage: 1,
    itemsCountPerPage: 1,
    pageRangeDisplayed: 5,
    totalItemsCount: 1
  });

  const [modal, setModal] = useState(false);
  const [etat, setEtat] = useState(true);
  const deleteRecord = record => {
    if (window.confirm("vous étes sur?")) {
      fetch(`/article/${record.id_article}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json, text/plain, */ /*",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          setEtat(!etat);
        });
    }
  };
  useEffect(() => {
    fetch("/article")
      .then(response => response.json())
      .then(data => {
        setArticle(data.data);
        //  window.alert(data);
        setDetail({
          activePage: data.current_page,
          itemsCountPerPage: data.per_page,
          totalItemsCount: data.total,
          pageRangeDisplayed: 3
        });
      });
  }, [etat]);
  const handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    //  setDetail({ activePage: pageNumber });
    fetch(`/article?page=${pageNumber}`)
      .then(response => response.json())
      .then(data => {
        setArticle(data.data);
        setDetail({
          activePage: data.current_page,
          itemsCountPerPage: data.per_page,
          totalItemsCount: data.total,
          pageRangeDisplayed: 3
        });
      });
  };
  const toggle = () => {
    setModal(!modal);
  };
  let columns = [
    {
      key: "ref",
      text: "reférence",
      className: "name",
      align: "center",
      sortable: true
    },
    {
      key: "designation",
      text: "designation",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "quantite",
      text: "quantité initiale",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "date",
      text: "date d'entrée",
      className: "rating",
      align: "center",
      sortable: true
    },
    {
      key: "alertstock",
      text: "stock alert",
      className: "rating",
      align: "center",
      sortable: true
    },
    {
      key: "unite",
      text: "unité",
      className: "type_of_food",
      sortable: true,
      align: "center"
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 160,
      align: "center",
      sortable: false,
      cell: record => {
        return (
          <Fragment>
            <Popup
              trigger={
                <button
                  className="btn btn-primary btn-sm"
                  /*   onClick={() => editRecord(record)}
                  style={{ marginRight: "5px" }}*/
                >
                  <i className="fa fa-edit" />
                </button>
              }
              modal
            >
              {close => (
                <div>
                  <a className="close" onClick={close}>
                    &times;
                  </a>
                  <UpdateArticle articles={record} />
                </div>
              )}
            </Popup>
            
            
             <a href={`/base/mouvement/${record.id_article}`}>   
               <button
                  className="btn btn-success btn-sm  mr-2 ml-2"
                 
                >

                  <i className="fa fa-map-signs" />
                  </button>
             </a>
             
            <button
              className="btn btn-danger btn-sm"
              onClick={() => deleteRecord(record)}
            >
              <i className="fa fa-trash" />
            </button>
          </Fragment>
        );
      }
    }
  ];
  return (
    <div className="animated fadeIn">
      
       <Row>
        <div className="card col-md-12">
          <div className="card-body" />
          <ReactDatatable
            className="table col text-center"
            config={config}
            records={article}
            columns={columns}
          />
          <Pagination
            activePage={detail.activePage}
            itemsCountPerPage={detail.itemsCountPerPage}
            totalItemsCount={detail.totalItemsCount}
            pageRangeDisplayed={detail.pageRangeDisplayed}
            onChange={handlePageChange}
          />
        </div>
      </Row>
    </div>
  );
};

export default Consultation;
