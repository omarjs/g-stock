import React, { useEffect, useState } from "react";
import ReactDatatable from "@ashvin27/react-datatable";
import Image from "material-ui-image";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Input
} from "reactstrap";

const Mouvement = props => {
  const [mouvements, setMouvements] = useState([]);
  useEffect(() => {
    fetch(`/mouvement/${props.articles.id_article}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        setMouvements(data);
      });
  });
  let columns = [
    {
      key: "id_mouvement",
      text: "id_mouvement",
      className: "name",
      align: "center",
      sortable: true
    },
    {
      key: "type",
      text: "type mouvement",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "quantite",
      text: "quantité",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "date",
      text: "date_mouvement",
      className: "postcode",
      align: "center",
      sortable: true
    }
  ];
  let config = {
    // page_size: 10,
    //length_menu: [10, 20, 50],
    show_pagination: false,
    button: {
      excel: false,
      print: false
    }
  };
  console.log(mouvements);
  return (
    <React.Fragment>
      <div className="row register-form  justify-content-center col-md-12  ">
        <div className="col-md-4">
          <Image src="/public/article/FqaMBobvn7.png" />
        </div>
        <div className="col-md-8 ">
          <div className="form-group mb-3">
            <ListGroup>
              <ListGroupItem>
                <Label>Designtaion : </Label>
                {props.articles.designation}
              </ListGroupItem>
              <ListGroupItem>
                <Label>Quantité : </Label>
                {props.articles.quantite}
              </ListGroupItem>
              <ListGroupItem>quantite réelle :</ListGroupItem>

              <ListGroupItem>
                <Label>Date : </Label>
                {props.articles.date}
              </ListGroupItem>
            </ListGroup>
          </div>
        </div>
        <div className="col-md-12" />
      </div>

      <div className="card w-100 ">
        <div className="card-body ">
          <ReactDatatable
            className="table col text-center"
            records={mouvements}
            columns={columns}
            config={config}
          />
        </div>
      </div>
    </React.Fragment>
  );
};
export default Mouvement;
