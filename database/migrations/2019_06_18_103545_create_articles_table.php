<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id_article');
            $table->string('ref');
            $table->string('designation');
            $table->date('date');
            $table->string('marque');
            $table->string('fabricant');
            $table->bigInteger('quantite')->nullable();
            $table->double('prix');
            $table->string('unite');
            $table->string('image');
            $table->bigInteger('alertstock')->nullable();
           
            

            $table->unsignedBigInteger('id_sousfamille');
            $table->foreign('id_sousfamille')->references('id_sousfamille')->on('sous_familles');

            $table->unsignedBigInteger('id_entrepot');
            $table->foreign('id_entrepot')->references('id_entrepot')->on('entrepots');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
       
    }
}
