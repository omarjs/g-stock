<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entrepot;
class EntrepotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $entrepot=Entrepot::all();
      return $entrepot->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
            $entrepot = new Entrepot;
            $entrepot->numero=$request->numero;
            $entrepot->name=$request->name;
            $entrepot->adresse=$request->adresse;
            $entrepot->save();
            return response()->json('entrepot ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $entrepot = Entrepot::where('id_entrepot',$id)->get();
        return response()->json($entrepot);
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json("edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


       Entrepot::where('id_entrepot',$request->id)->update(['numero'=>$request->numero,'name'=>$request->name,'adresse'=>$request->adresse]);
       return response()->json('ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entrepot=Entrepot::where('id_entrepot', $id);
        
       $entrepot->delete();
        
        return response()->json($id);
    }
}
