<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Mouvement;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Hash;
class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $article =Article::paginate(5);
       // $count=Article::all()->count();
        return  $article;
    }
    protected function getFileName($file)
    {
    return str_random(32) . '.' . $file->extension();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      foreach ($request->article as $a) 
      {   
      $article = new Article;
      $article->ref=$a["ref"];
      $article->designation =$a["designation"];
      $article->date= $a["date"];
      $article->marque= $a["marque"];
      $article->quantite= $a["quantite"];
      $article->alertstock= $a["stock"];
      $article->unite= $a["unite"];
      $article->id_sousfamille= $a["sousfamille"];
      $article->id_entrepot=$a["entrepot"];
      $Name = str_random(10).'.'.'png';
      $article->image=$Name;
      $image = $a["imagefile"]; // your base64 encoded
      $image = str_replace('data:image/png;base64,', '', $image);
      $image = str_replace(' ', '+', $image);
      File::put(base_path('public/public'). '/' . $Name, base64_decode($image));
      $article->save();
      $mouvement = new Mouvement;
      $mouvement->type='entree';
      $mouvement->date=$a["date"];
      $mouvement->quantite=$a["quantite"];
      $mouvement->id_article=$a["quantite"];
      $mouvement->article()->associate($article);
      $mouvement->save();
      }
    
    return response()->json("Product Added Successfully.");
      }
    /**
     * Display the specified resource.100
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
     $article = Article::where('ref', $id)->get();
     if(count($article) > 0)
   {
           
            return response()->json("no");
    }
        
            return response()->json("yes");
      

        
        //return Post::where('title', 'Post Two')->get();
    }   
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return response()->json("edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function display($id)
    {
       
        $article=Article::where('id_article', $id)->first();
        return  response()->json($article);

    }
    public function update(Request $request, $id)
    {
        Article::where('id_article',$request->id)->update(['ref'=>$request->ref,'date'=>$request->date,'designation'=>$request->designation,'marque'=>$request->marque,'quantite'=>$request->quantite,'unite'=>$request->unite,'alertstock'=>$request->alertstock,'id_sousfamille'=>$request->sousfamille,'id_entrepot'=>$request->entrepot]);
        return response()->json("l'article est bien modifie");
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article=Article::where('id_article', $id);
        $article->delete();
        return response()->json("ok is deleted");
    }
}
