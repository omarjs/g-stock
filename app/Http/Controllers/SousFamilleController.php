<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SousFamille;
use DB;
class SousFamilleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sousfamille=SousFamille::all();
        return $sousfamille->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $sousfamille = new SousFamille;
            $sousfamille->nom=$request->nom;
            $sousfamille->id_famille=$request->id_famille;
            
            $sousfamille->save();
        return response()->json("sousfamille est bien ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
       $sousfamille = SousFamille::where('id_famille',$id)->get();
       //return Post::where('title', 'Post Two')->get();
         return response()->json($sousfamille);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        SousFamille::where('id_sousfamille',$request->id)->update(['nom'=>$request->nom,'id_famille'=>$request->idfamille]);
        
        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sousfamille=SousFamille::where('id_sousfamille', $id);
        
        $sousfamille->delete();
        $sousfamille=SousFamille::all();
         return response()->json($sousfamille);
    }
}
