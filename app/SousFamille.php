<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SousFamille extends Model
{
    protected $primarykey ='id_sousfamille';
}
