<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $primarykey ='id_article';

    public function mouvements()
    {

        return $this->haMany('App\Mouvement', 'id_mouvement');
    }
}
