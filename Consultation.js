import React, { useState, useEffect } from "react";
import Popup from "reactjs-popup";

import {
  Badge,
  Media,
  Row,
  NavLink,
  Col,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Label,
  Input,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";

const Consultation = () => {
  const [article, setArticle] = useState([]);

  useEffect(() => {
    fetch("/article")
      .then(response => response.json())
      .then(data => {
        setArticle(data);
      });
  }, []);

  return (
    <div className="animated fadeIn">
      <Row>
        <div className="card col-md-12">
          <div className="card-header">les articles dans le stock</div>
          <div className="card-body" />
          <Table hover bordered striped responsive size="sm">
            <thead>
              <tr class="text-center">
                <th>Reférence</th>
                <th>Marque</th>
                <th>quantité initiale</th>
                <th>Stock Alert</th>
                <th>Date D'entrée</th>
                <th>Unité</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {article.map((a, index) => (
                <tr class="text-center" key={index}>
                  <td>{a.ref}</td>
                  <td>{a.marque}</td>
                  <td>{a.quantite}</td>
                  <td>{a.alertstock}</td>
                  <td>{a.date}</td>
                  <td>{a.unite}</td>
                  <td>
                    <Popup trigger={<NavLink href="#">image</NavLink>} modal>
                      {close => (
                        <div>
                          <a className="close" onClick={close}>
                            &times;
                          </a>
                          <div>
                            <img src="./1029.jpg" />
                          </div>
                        </div>
                      )}
                    </Popup>
                  </td>
                  <td />
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Row>
    </div>
  );
};

export default Consultation;
