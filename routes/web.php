<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('/base/consultation', function () {
    return view('home');
});
Route::get('/base/mouvement/{id}', function () {
   
    return view('home');
});
Route::get('/base/tabs', function () {
    return view('home');
});
Route::get('/base/articles', function () {
    return view('home');
});

Route::get('/base/gestion', function () {
    return view('home');
});
Route::get('/dashboard', function () {
    return view('home');
});
 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/article', 'ArticleController');
Route::resource('/entrepot', 'EntrepotController');
Route::resource('/sousfamille', 'SousFamilleController');
Route::resource('/famille', 'FamilleController');
Route::resource('/mouvement', 'MouvementController');
Route::get('/article/display/{id}','ArticleController@display');